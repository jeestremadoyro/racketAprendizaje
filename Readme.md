# Overview

Exercises adapted from a languages class in scheme. Commented for learning purposes

#Objective

To compile simple programs that teach scheme through comments

#Como correr los programas

Estando en una terminal
racket -t programs/<nombre del programa>.rkt

//verificar
o estando en una terminal de racket
(load "programs/nombre del programa")

#Como probarlo
Estando en una terminal
racket -t programs/<nombre del programa>.rkt

#Programs

[Next Day - program](programs/nextDay.rkt)
Function that receives three numbers
Themes seen in this program:
cond

[Next Day - Test File](tests/nextDayTest.rkt)
[Next Day - Docs](docs/nextDayDocs.md)


