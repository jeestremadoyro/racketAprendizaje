#!/usr/bin/racket
#lang racket

; En cada test de un programa de racket, ponemos varios tests. 
; El resultado de estos se compara con el resultado correcto
; En este script solo se esta testeando un caso por escenario
; Lo ideal seria probar varios

; Importamos el programa a testear
(require "../programs/nextDay.rkt")
(require "tests.rkt")

; Aqui comparamos para checar que tests fallaron
(prueba "Caso 1 - Dia antes del final del mes" 
        (dia-siguiente 12 9 2014) '(13 9 2014))
(prueba "Caso 2 - Ultimo dia de un mes con 31 dias" 
        (dia-siguiente 31 7 2013) '(1 8 2013))
(prueba "Caso 3 - Ultimo dia de un mes con 30 dias" 
        (dia-siguiente 30 6 2018) '(1 7 2018))
(prueba "Caso 4 - Ultimo dia del año" 
        (dia-siguiente 31 12 2010) '(1 1 2011))
(prueba "Caso 5 - Ultimo dia de Febrero en un año bisiesto" 
        (dia-siguiente 28 2 2000) '(29 2 2000))
(prueba "Caso 6 - Ultimo dia de Febrero en un año no bisiesto"
        (dia-siguiente 28 2 2001) '(1 3 2001))
